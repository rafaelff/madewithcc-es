\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{myclass}[]

%% Just use the original class and pass the options
\LoadClassWithOptions{report}

% Place page number at the outer foot edge of content pages
\usepackage[pagestyles]{titlesec}
\newpagestyle{mystyle}{\setfoot[\thepage][][]{}{}{\thepage}}
\pagestyle{mystyle}

\renewcommand{\baselinestretch}{1.2}
\newcommand{\beforetitle}{}
\newcommand{\publisherlogos}{}

\makeatletter

\usepackage{hyphenat}
%
% Customize the titlepage: remove the date, place the publisher name, and
% load a specific file for the verso page, containing some legal notices
%
\def\maketitle{%
  \titlerecto%
  \titleverso}

\def\titlerecto{\begin{titlepage}%
  \beforetitle
  \null\vfil
  \thispagestyle{empty}
  \vskip 160\p@
  \begin{center}%
    {\LARGE \@title \par}%
    \vskip 3em%
    {\Large
     \lineskip .75em%
      \begin{tabular}[t]{c}%
        \@author
      \end{tabular}\par}%
      \vskip 20em%
    {\large \DBKpublisheraddress \par}
    \vskip .75em%
    {\large \DBKpublishername \par}%
    \publisherlogos
  \end{center}\par
  \vfil\null
  \end{titlepage}}%

\def\titleverso{%
  \def\titlepagefile{titlepg.input.tex}
  \IfFileExists{\titlepagefile}{\input{\titlepagefile}}{}
}%

% We have specified @openright to allow the chapters to start in
% either page (recto or verso), but we don't want that behavior for
% parts - The part heading should start in the right page (recto)
% always.
%
% When a \part clears the preceding page, we don't want the page
% number in it either.

\renewcommand\part{%
  \clearpage
  \thispagestyle{empty}%
  \ifodd\value{page}
    \null%
  \else
    \null\newpage\thispagestyle{empty}%
  \fi
  \null\vfil
  \secdef\@part\@spart}

\def\@endpart{\vfil\newpage}

% Allow line break on hyphen (-) in URLs, to make sure long URLs in
% footnotes do not pass out of the margins.
\usepackage[hyphens]{url}

\newenvironment{colophon}{
  \pagebreak %
% FIXME change when page size changes, use {x}{x*1.2}

% Note, these numbers are not correct any more for the sizes mentioned:
%  \fontsize{6.5}{7.8}\selectfont % fits in one 4.25x6.875" pocket size page
%  \fontsize{7.5}{9}\selectfont % fits in one 5.06x7.71" size page
%  \fontsize{9.1}{10.92}\selectfont % fits in one 5.5x8.5" digest size page

  \fontsize{8.2}{9.84}\selectfont % fits in one 6x9'' size page
  \setlength{\parskip}{0.5em} %
  \setlength{\parindent}{0pt} %
}{}

\usepackage{titlesec}
\titleformat{\chapter}{\huge}{\thechapter.}{20pt}{\hyphenpenalty=10000 \Huge}
\titleformat{\section}{\Large}{\thesection}{1em}{}
\titleformat{\subsection}{\large}{\thesubsection}{1em}{}
%% \titleformat{\paragraph}{\normalsize}{\theparagraph}{1em}{}
%% \titleformat{\subparagraph}{\normalsize}{\thesubparagraph}{1em}{}

\usepackage{titletoc}

% indented subsection (in toc)
\titlecontents{part}
[0.0cm]             % left margin
{\vskip 0.5em}     % above code
{%                  % numbered format
{\Large\thecontentslabel\quad}%
}%
{\Large}         % unnumbered format
{\hfill \contentspage \vskip 0.2em}         % filler-page-format, e.g dots


% indented subsection (in toc)
\titlecontents{chapter}
[0.0cm]             % left margin
{}                  % above code
{%                  % numbered format
{\large\thecontentslabel.\quad}%
}%
{\large}         % unnumbered format
{\hfill \contentspage}         % filler-page-format, e.g dots

% We have many short chapters. Avoid them all starting on right-side
% pages, as it wastes too much space
\let\cleardoublepage\clearpage 
