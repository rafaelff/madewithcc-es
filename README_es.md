# Traducción al español de «Made With Creative Commons»

Este documento contempla los detalles específicos a la traducción al
español; sugerimos referirse al [archivo README global](./README.md)
para información respecto al proyecto.

Para colaborar con la traducción, únase al
[proyecto en Weblate](https://hosted.weblate.org/projects/madewithcc/translation/es/).

## Permisos

Estamos intentando realizar una impresión del libro traducido con el apoyo del
[Instituto de Investigaciones Económicas de la UNAM](http://www.iiec.unam.mx/).
Si va a realizar una traducción sustancial (digamos, una página de texto
impreso o más), agradeceremos que llene (todos los campos resaltados en
amarillo), firme, escanee y suba la
[liberación de permisos para la Universidad](../../docs/coautores_unam.odt).

## Normas para la traducción

Para realizar una traducción consistente y neutral, quienes contribuyan con
este proyecto deben seguir las
[normas de traducción de Debian](https://www.debian.org/international/spanish/notas),
además de estas normas específicas:

 * La traducción debe usar un género neutro, lo más posible. Por ejemplo,
   «creators» se traduce como «personas creadoras».
 * Los nombres de proyecto no se traducen, aunque sean palabras traducibles.
 * Al traducir segunda persona singular ("you" singular) se traduce
   por el formal "usted", dado que ambas versiones informales (tanto
   "tú" como "vos") resultan poco naturales para una amplia porción de
   los hispanoparlantes. La segunda persona plural se traduce por
   "ustedes" (no "vosotros") por la misma razón.

## Traducciones comunes

| inglés                              | español                                |
|-------------------------------------|----------------------------------------|
| Made with Creative Commons          | Hecho con Creative Commons             |
| commons                             | procomún                               |
| link                                | enlace                                 |
| funding                             | financiamiento                         |

## Dudas

En caso de tener una duda de traducción que no esté especificada en las normas
de traducción de Debian ni en este documento, puede
[reportarla](https://gitlab.com/gunnarwolf/madewithcc-es/issues) para que sea
discutida por quienes colaboran con el proyecto.

## Disputas

Si no hay acuerdo en la forma que debe seguir alguna de las traducciones, la
forma que definan [Gunnar Wolf](https://gitlab.com/gunnarwolf/) y
[Leo Arias](https://gitlab.com/elopio/) será aplicada en todo el libro.
