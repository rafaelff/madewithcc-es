<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:import href="http://docbook.sourceforge.net/release/xsl/current/fo/docbook.xsl"/>

  <xsl:include href="pdf.xsl" />
  <xsl:param name="fop1.extensions">1</xsl:param>

  <xsl:param name="page.width">8.5in</xsl:param>
  <xsl:param name="page.height">11in</xsl:param>

  <xsl:param name="page.margin.inner">1.5in</xsl:param>
  <xsl:param name="page.margin.outer">1.5in</xsl:param>

  <xsl:param name="page.margin.top">1in</xsl:param>
  <xsl:param name="page.margin.bottom">1in</xsl:param>

  <xsl:param name="preface.tocdepth">1</xsl:param>
  <xsl:param name="toc.section.depth">1</xsl:param>

</xsl:stylesheet>
