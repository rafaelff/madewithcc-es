#!/usr/bin/ruby
# coding: utf-8

require 'nokogiri'

LogLevel=1
raise ArgumentError, 'Language and source/destination files not specified' if ARGV.size != 3
lang = ARGV[0]
srcfile = ARGV[1]
dstfile = ARGV[2]

f=File.open(srcfile)
xml = Nokogiri::XML(f)

def log(level,what)
  indicators = %w(! • # -)
  if level >= LogLevel
    print indicators[level]
  else
    print "\n%s %s\n" % ['*' * (level+1), what]
  end
end

if intro = File.open('extra/%s/intro_%s.xml' % [lang, lang],
                     'r').read rescue nil
  log 0, 'Including the %s version introduction' % lang
  xml.search('preface').last.after( intro )
else
  log 0, ' -!- Not including the %s introduction text' % lang
end

if 'es' == lang
  log 0, 'Final editorial requests: Attributions on top, as in other chapters, also for prefaces'
  xml.search('preface').each do |pref|
    title = pref.search('title').first
    attrib = pref.search('blockquote').last
    attrib_text =  attrib.inner_text.gsub(/\s+/,' ')

    # Some formatting issues we need to modify
    attrib_text.gsub!(/Paul y Sarah/, 'Paul <?latex \textup{>y<?latex } > Sarah')
    attrib_text.gsub!(/Merkley,? (CEO.*)/, 'Merkley <citetitle>\1</citetitle>')
    attrib_text.gsub!(/Wolf (Ins.*)/, 'Wolf <citetitle>\1</citetitle>')

    attrib.remove
    title.after('<blockquote><attribution>%s</attribution><para/></blockquote>' % attrib_text)
    log 1, 'Moved: %s' % attrib_text
  end
end

if legal = File.open('extra/%s/legal_info.xml' % lang, 'r').read rescue nil
  log 0, 'Replace legal info by the information assembled by our publisher'
  xml.search('colophon').first.inner_html = legal
else
  log 0, ' -!- Not including the %s legal_info text' % lang
end

# Doing this the dirty, non-DocBooky way: I'm adding a large <?latex
# ... ?> section to the last appendix. This should probably go in the
# <colophon> section, but it's already used.
#
# A cleaner way would be to replace the <xsl:param
# name="latex.enddocument"> section in pdf_es.xsl — But it gets
# trickier :-Þ
if colophon = File.open('extra/%s/colophon.tex' % lang, 'r').read rescue nil
  log 0, 'Add a colophon at the end'
  xml.search('appendix').last.search('para').last.after('<?latex %s ?>' % colophon)
else
  log 0, ' -!- Not including the %s colophon text' % lang
end


log 0, 'Writing processed file'
# Unable to figure out API way to replace DOCTYPE
data = xml.to_xml()
File.open(dstfile, 'w') {|f| f.write(data)}
